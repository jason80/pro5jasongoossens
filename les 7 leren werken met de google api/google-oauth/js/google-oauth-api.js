isSignedIn();
showSignInOrOutButtons();
console.log(isSignedIn());

function displayWelcome(profile) {
  let userSpan = document.createElement("span");
  userSpan.id = "user";
  let textContent = document.createTextNode(
    "Welcome back, " + profile.getName()
  );
  userSpan.appendChild(textContent);
  document
    .getElementById("nav")
    .insertBefore(userSpan, document.getElementById("btns"));
}

function displayNoUserLoggedIn() {
  if (document.getElementById("user-info-container") !== null) {
    document.getElementById("user-info-container").innerHTML =
      "No user logged in";
  }
}

function removeWelcome() {
  if (document.getElementById("user") !== null) {
    document.getElementById("user").remove();
  }
}

function showSignInOrOutButtons() {
  if (isSignedIn()) {
    document.getElementsByClassName("g-signin2")[0].style.display = "none";
    document.getElementById("sign-out-btn").style.display = "inline-block";
  } else {
    document.getElementById("sign-out-btn").style.display = "none";
    document.getElementsByClassName("g-signin2")[0].style.display =
      "inline-block";
  }
}

function displayUserInfo(profile) {
  if (document.getElementById("user-info-container") !== null) {
    let container = document.getElementById("user-info-container");
    container.innerHTML = "";

    let userHeader = document.createElement("h1");
    userHeader.innerHTML = "User";

    let userName = document.createElement("p");
    userName.id = "user-name";
    userName.innerHTML = "Name: " + profile.getName();

    let userEmail = document.createElement("p");
    userEmail.id = "user-email";
    userEmail.innerHTML = "Email: " + profile.getEmail();

    let userImage = document.createElement("img");
    userImage.id = "user-image";
    userImage.src = profile.getImageUrl();

    container.appendChild(userHeader);
    container.appendChild(userName);
    container.appendChild(userEmail);
    container.appendChild(userImage);
  }
}

function onSignIn(googleUser) {
  var profile = googleUser.getBasicProfile();

  localStorage.setItem("loggedIn", true);

  displayWelcome(profile);
  displayUserInfo(profile);
  isSignedIn();
  showSignInOrOutButtons();
}

function isSignedIn() {
  if (localStorage.getItem("loggedIn")) {
    return true;
  } else {
    displayNoUserLoggedIn();
    return false;
  }
}

function onSignOut() {
  gapi.auth2.getAuthInstance().signOut();

  if (isSignedIn()) {
    removeWelcome();
    displayNoUserLoggedIn();
  }
  localStorage.removeItem("loggedIn");
  isSignedIn();
  showSignInOrOutButtons();
}
