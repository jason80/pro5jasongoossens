let title = document.querySelector(".title");
title.style.color = "black";

function titleChange() {
  if (title.style.color === "black") {
    title.style.color = "grey";
    console.log("grey");
  } else {
    title.style.color = "black";
    console.log("black");
  }
}

const mymap = L.map("mapid").setView([51.2513, 4.4486], 15);
const attribution =
  '© <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>';
const tileUrl = `http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png`;

const tiles = L.tileLayer(tileUrl, { attribution });

tiles.addTo(mymap);

// https://www.youtube.com/watch?v=nZaZ2dB6pow
// https://www.google.com/search?newwindow=1&rlz=1C1MSIM_enBE686BE692&sxsrf=ALeKk032mTT_iSza5U00i5Ma_BkSWodYaQ:1582298853655&q=supermarkten+merksem&npsic=0&rflfq=1&rlha=0&rllag=51251996,4449019,867&tbm=lcl&ved=2ahUKEwjaxObI-uLnAhVNqaQKHWPmAycQjGp6BAgMEEQ&tbs=lrf:!1m4!1u3!2m2!3m1!1e1!1m4!1u2!2m2!2m1!1e1!1m4!1u16!2m2!16m1!1e1!1m4!1u16!2m2!16m1!1e2!2m1!1e2!2m1!1e16!2m1!1e3!3sIAE,lf:1,lf_ui:10&rldoc=1#rlfi=hd:;si:;mv:[[51.270919161496415,4.515497396288993],[51.22998230310622,4.386751363574149],null,[51.25045528776393,4.451124379931571],14]

const request = new XMLHttpRequest();

request.open("GET", "/resources/map.json", true); //  in resources folder: npx http-server
request.onload = function() {
  const data = JSON.parse(this.response);

  const supermarket = data.supermarket.map(supermarket => {
    L.marker([supermarket.lat, supermarket.long])
      .bindPopup(
        `<h2>${supermarket.name}<h2>
      <p>Street: ${supermarket.street}</p>
      <p>Rating: ${supermarket.rating}</p>
      <p>Comment: ${supermarket.comment}</p>     
      `
      )
      .openPopup()
      .addTo(mymap);
  });

  const individualStreetCount = data.supermarket.reduce((sums, supermarket) => {
    sums[supermarket.street] = (sums[supermarket.street] || 0) + 1;
    return sums;
  }, {});

  const streetContainer = document.getElementById("streetnames");

  for (const street in individualStreetCount) {
    const p = document.createElement("p");
    p.innerHTML = `<b>${street}<b>: ${individualStreetCount[street]}`;
    streetContainer.append(p);
  }
};

request.send();
