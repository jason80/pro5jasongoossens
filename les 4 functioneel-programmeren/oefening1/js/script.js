function init(array) {
  let i = 0;
  return function() {
    return array[i++];
  };
}

const testArray = [1, 5, 10, 15];

const volgende = init(testArray);
let appendedText = document.getElementById("content");
appendedText.innerHTML += volgende() + "<br>";
appendedText.innerHTML += volgende() + "<br>";
appendedText.innerHTML += volgende() + "<br>";
appendedText.innerHTML += volgende() + "<br>";
appendedText.innerHTML += volgende() + "<br>";
