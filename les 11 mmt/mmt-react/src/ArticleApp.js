import React, { Component } from "react";
import ArticleOverview from "./ArticleOverview";

class App extends Component {
  render() {
    return <ArticleOverview items={this.props.items} />;
  }
}
export default App;
