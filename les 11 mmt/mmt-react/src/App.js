import React, { Component } from "react";
import Header from "./Header";
import ArticleApp from "./ArticleApp";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoaded: false,
      data: [],
    };
  }

  style = {
    h3: { margin: "2em", fontStyle: "italic" },
  };

  componentDidMount() {
    const url = "./data/neolithicum.json";

    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        setTimeout((t) => this.setState({ isLoaded: true, data }), 500);
      })
      .catch((error) => alert("JSON kon niet worden geladen\n" + error));
  }

  render() {
    const { data, isLoaded } = this.state;
    if (!isLoaded) {
      return <h3 style={this.style.h3}>Loading...</h3>;
    } else {
      return (
        <React.Fragment>
          <Header title={data.title} imageUrl={data.logoUrl} />
          <ArticleApp items={data.curiosity} />
        </React.Fragment>
      );
    }
  }
}

export default App;
