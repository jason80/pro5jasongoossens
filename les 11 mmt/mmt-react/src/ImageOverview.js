import React, { Component } from "react";

class ImageOverview extends Component {
  state = {};

  style = { borderRadius: "6px", margin: "2px" };

  render() {
    const { item } = this.props;

    return (
      <React.Fragment>
        <img
          style={this.style}
          src={`./images/small/${item.image}`}
          alt={`Afbeelding ${item.name}`}
        />
      </React.Fragment>
    );
  }
}

export default ImageOverview;
