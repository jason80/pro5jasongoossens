import React, { Component } from "react";

class ImageDetail extends Component {
  state = {};

  style = {
    height: "100%",
    maxWidth: "auto",
    display: "block",
    
  };

  render() {
    const { name, image } = this.props;

    return (
      <React.Fragment>
        <img
          style={this.style}
          src={`./images/big/${image}`}
          alt={`Afbeelding ${name}`}
        />
      </React.Fragment>
    );
  }
}

export default ImageDetail;
