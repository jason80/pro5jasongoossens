import React, { Component } from "react";

class MyButton extends Component {
  state = {};

  style = {
    boxShadow: "inset 0px 1px 0px 0px grey",
    background: "linear-gradient(to bottom, grey 5%, darkgrey 100%)",
    backgroundColor: "black",
    borderRadius: "6px",
    border: "1px solid black",
    display: "inline-block",
    cursor: "pointer",
    color: "#ffffff",
    fontFamily: "Arial",
    fontSize: "15px",
    fontWeight: "bold",
    margin: "2px",
    padding: "6px 24px",
    textShadow: "0px 1px 0px black",
    width: "calc(10rem - 2px)",
  };

  render() {
    const { caption, onClick } = this.props;
    return (
      <div>
        <button style={this.style} onClick={onClick}>
          {typeof caption !== "undefined" ? caption : "Button"}{" "}
        </button>
      </div>
    );
  }
}

export default MyButton;
