import React, { Component } from "react";
import ImageOverview from "./ImageOverview";
import MyButton from "./MyButton";

class ArticleSummary extends Component {
  state = {};

  style = {
    display: "flex",
    flexDirection: "column",
    width: "10em",
  };

  render() {
    const { item, showDetail } = this.props;

    return (
      <article style={this.style}>
        <ImageOverview item={item} />
        <MyButton caption={item.name} onClick={showDetail} />
      </article>
    );
  }
}

export default ArticleSummary;
