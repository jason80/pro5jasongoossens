import React, { Component } from "react";

class Header extends Component {
  style = {
    header: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      width: "100%",
      height: "10em",
      paddingBottom: "1em",
      borderBottom: "1px solid black",
    },
    title: {
      alignSelf: "center",
      fontFamily: "Arial",
      color: "red",
      paddingRight: "1em",
    },
    logo: {
      height: "10em",
    },
  };
  render() {
    const { imageUrl, title } = this.props;
    return (
      <header style={this.style.header}>
        <img style={this.style.lolg} src={imageUrl} alt={`Logo ${title}`} />
        <h1 style={this.style.title}>{title}</h1>
      </header>
    );
  }
}

export default Header;
