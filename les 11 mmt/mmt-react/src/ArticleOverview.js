import React, { Component } from "react";
import ArticleSummary from "./ArticleSummary";
import ArticleDetail from "./ArticleDetail";

class ArticleOverview extends Component {
  state = {
    detail: false,
    article: null,
  };

  style = {
    color: "white",
    display: "flex",
    flexWrap: "wrap",
    alignItems: "baseline",
  };

  handleShowDetail = (item) => {
    this.setState({ detail: true, article: item });
  };

  handleShowOverview = () => {
    this.setState({ detail: false, article: null });
  };

  render() {
    const { items } = this.props;

    if (!this.state.detail) {
      return (
        <main>
          <div style={this.style}>
            {items.map((item) => (
              <ArticleSummary
                key={item.key}
                item={item}
                showDetail={() => this.handleShowDetail(item)}
              />
            ))}
          </div>
        </main>
      );
    } else {
      return (
        <ArticleDetail
          item={this.state.article}
          showOverview={() => this.handleShowOverview()}
        />
      );
    }
  }
}

export default ArticleOverview;
