import React, { Component } from "react";
import MyButton from "./MyButton";
import ImageDetail from "./ImageDetail";
import LeafletNode from "./LeafletNode";
import L from "leaflet";
import "leaflet/dist/leaflet.css";

class ArticleDetail extends Component {
  state = {};

  style = {
    top: {
      display: "flex",
      flexDirection: "row",
      justifyContent: "space-between",
      width: "90%",
      height: "45vh",
      margin: "8px",
    },
    middle: {
      display: "flex",
      flexDirection: "row",
    },
    comment: {
      display: "block",
    },
  };

  componentDidMount() {
    this.leafletView("leaflet-id");
  }

  leafletView = (id) => {
    const map = L.map(id);
    const baseMap = L.tileLayer(
      "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
      {
        maxZoom: 19,
        attribution:
          '© <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
      }
    );
    baseMap.addTo(map);
    const position = [
      this.props.item.latitude.substr(0, 5),
      this.props.item.longitude.substr(0, 5),
    ];

    L.marker([this.props.item.latitude, this.props.item.longitude]).addTo(map);

    map.setView(position, 12);
  };

  render() {
    const { showOverview, item } = this.props;

    return (
      <article style={this.style}>
        <MyButton caption="Terug naar overzicht" onClick={showOverview} />
        <div style={this.style.top}>
          <ImageDetail name={item.name} image={item.image} />
          <LeafletNode />
        </div>
        <div style={this.style.middle}>
          <div>
            <ul>
              <li>{item.name}</li>
              <li>{item.type}</li>
              <li>{item.period}</li>
              <li>{item.country}</li>
              <li>{item.region}</li>
              <li>{item.city}</li>
            </ul>
          </div>
          <div>
            <ul>
              <li>{item.coordinates}</li>
              <li>{item.longitude}</li>
              <li>{item.latitude}</li>
            </ul>
            {/* <LikePanel /> */}
          </div>
        </div>
        <textarea style={this.style.comment}></textarea>
        <MyButton caption="Verzenden" />
      </article>
    );
  }
}

export default ArticleDetail;
