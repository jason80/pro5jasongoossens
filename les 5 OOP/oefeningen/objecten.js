let myArray = ["appel", "peer", "ananas", "sinaasappel", "banaan"];

let lineBreak = "<br>";

document.getElementById("algemeen").innerHTML = myArray + lineBreak;

document.getElementById("algemeen").innerHTML += myArray[3] + lineBreak;

let persoon = {
  naam: "Goossens",
  voornaam: "Jason",
  leeftijd: 40,
  intro: function() {
    return (
      this.voornaam + " " + this.naam.toUpperCase() + " is " + this.leeftijd
    );
  }
};

document.getElementById("persoon").innerHTML =
  persoon.voornaam + " " + persoon.naam;
document.getElementById("persoon").innerHTML +=
  " is " + persoon.leeftijd + lineBreak;
document.getElementById("persoon").innerHTML += persoon.intro();

// https://www.youtube.com/watch?v=X0ipw1k7ygU
// https://www.youtube.com/watch?v=pTB0EiLXUC8
// https://www.youtube.com/watch?v=vDJpGenyHaA
// https://www.youtube.com/watch?v=napDjGFjHR0
