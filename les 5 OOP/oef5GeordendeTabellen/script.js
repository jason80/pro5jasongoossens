function TableSort(tableId) {
  this.tableElement = document.getElementById(tableId);
  if (this.tableElement && this.tableElement.nodeName == "TABLE") {
    this.prepare();
  }
}

TableSort.prototype.prepare = function() {
  const headings = this.tableElement.tHead.rows[0].cells; // all headers in var
  for (let i = 0; i < headings.length; i++) {
    headings[i].innerHTML += "<span>&nbsp;&nbsp;&uarr;</span>";
    headings[i].className = "asc";
  }
  this.tableElement.addEventListener(
    "click",
    (function(that) {
      return function(event) {
        that.eventHandler(event);
      };
    })(this),
    false
  );
};

TableSort.prototype.eventHandler = function(event) {
  if (
    event.target.tagName === "SPAN" &&
    event.target.parentNode.tagName === "TH"
  ) {
    if (event.target.parentNode.className == "asc") {
      event.target.parentNode.className = "desc";
      event.target.innerHTML = "&nbsp;&nbsp;&darr;";
      console.log(event.target.parentNode.className);
      this.sortColumn(event.target.parentNode);
    } else {
      event.target.parentNode.className = "asc";
      event.target.innerHTML = "&nbsp;&nbsp;&uarr;";
      console.log(event.target.parentNode.className);
      this.sortColumn(event.target.parentNode);
    }
  }
};

TableSort.prototype.sortColumn = function(headerCell) {
  let rows = this.tableElement.rows;
  let alpha = [],
    numeric = [];
  let alphaIndex = 0,
    numericIndex = 0;
  let cellIndex = headerCell.cellIndex;
  for (var i = 1; rows[i]; i++) {
    let cell = rows[i].cells[cellIndex];
    let content = cell.textContent ? cell.textContent : cell.innerText;
    let numericValue = content.replace(/(\$|\,|\s)/g, "");
    if (parseFloat(numericValue) == numericValue) {
      numeric[numericIndex++] = {
        value: Number(numericValue),
        row: rows[i]
      };
    } else {
      alpha[alphaIndex++] = {
        value: content,
        row: rows[i]
      };
    }
  }
  let orderdedColumns = [];
  numeric.sort(function(a, b) {
    return a.value - b.value;
  });
  alpha.sort(function(a, b) {
    let aName = a.value.toLowerCase();
    let bName = b.value.toLowerCase();
    if (aName < bName) {
      return -1;
    } else if (aName > bName) {
      return 1;
    } else {
      return 0;
    }
  });
  orderdedColumns = numeric.concat(alpha);
  let tBody = this.tableElement.tBodies[0];
  for (let i = 0; orderdedColumns[i]; i++) {
    tBody.appendChild(orderdedColumns[i].row);
  }
};

window.onload = function() {
  const jommeke = new TableSort("jommeke");
  const fruit = new TableSort("fruit");
};
