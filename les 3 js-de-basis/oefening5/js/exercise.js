document.addEventListener("DOMContentLoaded", function(event) {
  const height = innerHeight;
  const width = innerWidth;
  const contentHeight = document.getElementsByClassName("content")[0]
    .offsetHeight;
  const contentWidth = document.getElementsByClassName("content")[0]
    .offsetWidth;
  console.log("Window borders:" + height + " " + width);

  function draw() {
    let canvas = document.getElementById("play-area");
    canvas.height = contentHeight;
    canvas.width = contentWidth;

    if (canvas.getContext) {
      const radius = Math.random() * 50;
      const color = "hotpink";
      const x = Math.floor(Math.random() * (contentWidth - radius));
      const y = Math.floor(Math.random() * (contentHeight - radius));
      console.log(x + " " + y);
      let ctx = canvas.getContext("2d");
      ctx.beginPath();
      ctx.arc(x, y, radius, 0, 2 * Math.PI);
      ctx.lineWidth = 3;
      ctx.strokeStyle = color;
      ctx.fillStyle = color;
      ctx.stroke();
      ctx.fill();
      return ctx;
      console.log("Circle");
    }
  }

  setInterval(draw, 500);
});
