﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace mmt_react_backend.Models
{
    public partial class mE3WNR0XfTContext : DbContext
    {
        public mE3WNR0XfTContext()
        {
        }

        public mE3WNR0XfTContext(DbContextOptions<mE3WNR0XfTContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Comment> Comment { get; set; }
        public virtual DbSet<LocationLike> LocationLike { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Comment>(entity =>
            {
                entity.ToTable("comment");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CommentText)
                    .HasColumnName("comment_text")
                    .HasMaxLength(2000)
                    .IsUnicode(false);

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Location)
                    .HasColumnName("location")
                    .HasColumnType("int(11)");
            });

            modelBuilder.Entity<LocationLike>(entity =>
            {
                entity.ToTable("location_like");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .HasColumnType("int(10)");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.LikeCount)
                    .HasColumnName("like_count")
                    .HasColumnType("int(10)");

                entity.Property(e => e.Location)
                    .HasColumnName("location")
                    .HasColumnType("int(11)");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
