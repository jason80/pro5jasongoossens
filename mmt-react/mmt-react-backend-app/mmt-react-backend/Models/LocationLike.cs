﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace mmt_react_backend.Models
{
    public partial class LocationLike
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int Location { get; set; }
        public int? LikeCount { get; set; }
        public DateTimeOffset? CreatedAt { get; set; }
    }
}
