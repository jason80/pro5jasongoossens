using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using mmt_react_backend.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace mmt_back_end.Controllers
{
    [Route("likes")]
    [ApiController]
    public class LikesController : ControllerBase
    {
        private readonly mE3WNR0XfTContext _context;
        public LikesController(mE3WNR0XfTContext context)
        {
            _context = context;
        }

        public async Task<ActionResult<IEnumerable<LocationLike>>> GetLikes()
        {
            return await _context.LocationLike.OrderBy(x => x.Id).ToListAsync();
        }

        // get all likes for one location
        [HttpGet("{location}")]
        public int GetLikesForLocation(int location)
        {
            var likes = _context.LocationLike.Where(c => c.Location == location).FirstOrDefault();

            return (int)likes.LikeCount;
        }

        // get likes per location
        [HttpGet("count")]
        public async Task<ActionResult<IEnumerable<object>>> GetLIkesCount()
        {
            var likes = _context.LocationLike
                        .Select(x => new
                        {
                            Id = x.Id,
                            LocationLike = x.Location,
                            LikeCount = x.LikeCount
                        })
                        .OrderBy(x => x.Id)
                        .Cast<object>()
                        .ToListAsync();

            return await likes;
        }

        // add one comment for a location
        [HttpPost]
        public LocationLike PostLikes(LocationLike like)
        {

            LocationLike existingLikes = _context.LocationLike.Where(c => c.Location == like.Location).FirstOrDefault();

            if (existingLikes == null)
            {
                _context.Add(like);
                _context.SaveChanges();
            }
            else
            {
                existingLikes.LikeCount = existingLikes.LikeCount + 1;
                _context.Update(existingLikes);
                _context.SaveChanges();

            }

            return like;
        }
    }
}