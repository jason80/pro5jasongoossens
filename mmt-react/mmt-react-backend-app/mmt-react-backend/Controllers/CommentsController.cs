using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using mmt_react_backend.Models;

namespace mmt_back_end.Controllers
{
    [Route("comments")]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly mE3WNR0XfTContext _context;

        public CommentsController(mE3WNR0XfTContext context)
        {
            _context = context;
        }

        // get all comments
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Comment>>> GetComments()
        {
            return await _context.Comment.OrderBy(x => x.Id).ToListAsync();
        }
        
        // get all comments for one location
        [HttpGet("{location}")]
        public async Task<ActionResult<IEnumerable<Comment>>> GetCommentsForLocation(int location)
        {
            var comments = await _context.Comment.Where(c => c.Location == location).ToListAsync();

            return comments;
        }

        // get comment count per location
        [HttpGet("count")]
        public async Task<ActionResult<IEnumerable<object>>> GetCommentsCount()
        {
            // cast to general object type because I'm not returning a Comment
            // also, GroupBy returns IGrouping object, not a Comment
            var comments = _context.Comment
                        .GroupBy(comment => comment.Location)
                        .Select(x => new { Id = x.Key, Count = x.Count() })
                        .OrderBy(x => x.Id)
                        .Cast<object>()
                        .ToListAsync();

            return await comments;

        }

        // add one comment for a location
        [HttpPost]
        public int PostComment(Comment comment)
        {
            _context.Add(comment);
            _context.SaveChanges();

            return comment.Id;
        }
    }
}
