﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ToDoApp.Models
{
    public partial class todoitemsContext : DbContext
    {
        public todoitemsContext()
        {
        }

        public todoitemsContext(DbContextOptions<todoitemsContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Todoitem> Todoitem { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Todoitem>(entity =>
            {
                entity.ToTable("todoitem");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Iscomplete)
                    .HasColumnName("iscomplete")
                    .HasColumnType("tinyint(1)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
