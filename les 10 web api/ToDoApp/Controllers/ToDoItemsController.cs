using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ToDoApp.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace ToDoApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemsController : ControllerBase
    {
        private readonly todoitemsContext _context;
        public ItemsController(todoitemsContext context)
        {
            _context = context;
        }

        // GET api/items
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Todoitem>>> GetToDoItems()
        {
            return await _context.Todoitem.ToListAsync();
        }

        // GET api/items/1
        [HttpGet("{id}")]
        public async Task<ActionResult<Todoitem>> GetToDoItem(int id)
        {
            var todoitem = await _context.Todoitem.FindAsync(id);

            if (todoitem == null)
            {
                return NotFound();
            }
            return todoitem;
        }

        [HttpPost]
        public async Task<ActionResult<int>> PostToDoItem(Todoitem todoitem)
        {
            _context.Todoitem.Add(todoitem);
            var returnVal = await _context.SaveChangesAsync();

            return todoitem.Id;
        }

        // PUT: api/items/5
        [HttpPut("{id}")]
        public async Task<ActionResult<int>> PutProduct(int id, Todoitem todoitem)
        {
            if (id != todoitem.Id)
            {
                return BadRequest();
            }

            _context.Entry(todoitem).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!!ToDoItemExists(id))
                {
                    return NotFound();

                }
                else
                {
                    throw;
                }
            }

            return todoitem.Id;
        }

        private bool ToDoItemExists(int id)
        {
            return _context.Todoitem.Any(e => e.Id == id);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<Todoitem>> DeleteToDoItem(int id)
        {
            var todoitem = await _context.Todoitem.FindAsync(id);
            if (todoitem == null)
            {
                return NotFound();
            }

            _context.Todoitem.Remove(todoitem);
            await _context.SaveChangesAsync();

            return todoitem;

        }
    }
}