class Like extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      likes: 0,
    };
  }

  handleIncrement = () => {
    let likes = this.state.likes + 1;
    this.setState({ likes });
  };

  render() {
    return (
      <div>
        <Button
          onIncrement={this.handleIncrement}
          caption={this.props.caption}
        ></Button>
        <Label likes={this.state.likes}></Label>
      </div>
    );
  }
}

class Button extends React.Component {
  state = {};

  buttonStyle = {
    boxShadow: "inset 0px 1px 0px 0px #97c4fe",
    background: "linear-gradient(to bottom, #3d94f6 5%, #1e62d0 100%)",
    backgroundColor: "#3d94f6",
    borderRadius: "6px",
    border: "1px solid #337fed",
    display: "inline-block",
    cursor: "pointer",
    color: "#ffffff",
    fontFamily: "Arial",
    fontSize: "15px",
    fontWeight: "bold",
    padding: "6px 24px",
    textShadow: "0px 1px 0px #1570cd",
    marginRight: "3px",
  };

  render() {
    return (
      <button
        onClick={() => {
          this.props.onIncrement();
        }}
        style={this.buttonStyle}
      >
        {this.props.caption}
      </button>
    );
  }
}

class Label extends React.Component {
  state = {};

  labelStyle = {
    display: "inline-block",
    color: "#ffffff",
    backgroundColor: "#3d94f6",
    textShadow: "0px 1px 0px #1570cd",
    padding: "6px 24px",
    textShadow: "0px 1px 0px #1570cd",
    borderRadius: "6px",
    border: "1px solid #337fed",
  };

  render() {
    return <label style={this.labelStyle}>{this.props.likes}</label>;
  }
}

export const Like