import Like from "../src/components/like";

class MarvelCharacter extends React.Component {
  e = React.createElement;
  marvelCharacterStyle = {
    list: {
      display: "flex",
      flexWrap: "wrap",
    },
    tile: {
      color: "white",
      backgroundColor: "red",
      textAlign: "center",
      width: "8em",
      fontSize: "2em",
      fontFamily: "Arial",
      paddingTop: "0.5em",
      margin: "0.1em",
      borderRadius: "0.3em",
      border: "3px solid black",
    },
    image: {
      color: "blue",
      width: "8em",
      paddingTop: "0.5em",
      borderRadius: "0.3em",
    },
  };

  MarvelCharacter(props) {
    return (
      <div key={props.name} style={this.marvelCharacterStyle.tile}>
        {props.name}
        <img
          src={props.imageUrl}
          style={this.marvelCharacterStyle.image}
          alt={"Foto van " + props.name}
        />
        <Like caption="Likes" />
      </div>
    );
  }

  render() {
    return (
      <div>
        <h1>{this.props.heading}</h1>
        <div style={this.marvelCharacterStyle.list}>
          {this.props.list.map((element) => this.MarvelCharacter(element))}
        </div>
      </div>
    );
  }
}
