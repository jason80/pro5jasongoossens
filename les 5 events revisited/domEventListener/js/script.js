let background = "hotpink";

function set(id) {
  background = id;
}

const colourParagraph = e => {
  if (e.target.tagName == "P") {
    if (e.target.classList.contains(background)) {
      e.target.removeAttribute("class");
    } else {
      e.target.removeAttribute("class");
      e.target.classList.add(background);
    }
  }
};

const div = document.getElementsByTagName("div")[0];
div.addEventListener("click", colourParagraph);
