const url =
  "https://gateway.marvel.com:443/v1/public/characters?limit=3&ts=1&apikey=7e2052014a6f88735a21368b38c4e8e4&hash=f4cc9e2f9c6fa2423c6c1758db5d2a0e";

function loadHeroesXML() {
  const req = new XMLHttpRequest();

  req.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      res = JSON.parse(this.responseText);
      showHeroes(res);
    }
  };

  req.open("GET", url, true);
  req.send();
}

function loadHeroesFetch() {
  fetch(url)
    .then((res) => res.json())
    .then((data) => {
      showHeroes(data);
    })
    .catch((err) => console.log("Error is: ", err));
}

function showHeroes(heroes) {
  const chars = heroes.data.results;
  const table = document.createElement("table");
  chars.map((character) => {
    let row = document.createElement("tr");
    let name = document.createElement("td");
    let desciption = document.createElement("td");
    let textContent = document.createTextNode(character.name);
    let descriptionContent = document.createTextNode(character.description);
    name.appendChild(textContent);
    desciption.appendChild(descriptionContent);
    row.appendChild(name);
    row.appendChild(desciption);
    table.appendChild(row);
  });

  document.getElementById("heroes-list").appendChild(table);
}
